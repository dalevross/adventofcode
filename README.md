# Advent Of Code 2019

[Day 1](https://adventofcode.com/2019/day/1) &#127775;&#127775; [Solution](day1/Program.cs)

[Day 2](https://adventofcode.com/2019/day/2) &#127775;&#127775; [Solution](day2/Program.cs)

[Day 3](https://adventofcode.com/2019/day/3) &#127775;&#127775; [Solution](day3/Program.cs)

[Day 4](https://adventofcode.com/2019/day/4) &#127775;&#127775; [Solution](day4/Program.cs)

[Day 5](https://adventofcode.com/2019/day/5) &#127775;&#127775; [Solution](day5/Program.cs)

[Day 6](https://adventofcode.com/2019/day/5) &#127775;&#127775; [Solution](day6/Program.cs)

[Day 7](https://adventofcode.com/2019/day/7) &#127775; [Solution](day7/Program.cs)

[Day 8](https://adventofcode.com/2019/day/8) &#127775;&#127775; [Solution](day8/Program.cs)

[Day 9](https://adventofcode.com/2019/day/8) [Solution](#)

[Day 10](https://adventofcode.com/2019/day/8) &#127775;&#127775;[Solution](day10/Program.cs)


